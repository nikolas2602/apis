﻿using Newtonsoft.Json;
using System;
using System.Net;
using ViaCEP.Models;

namespace ViaCEP
{
    class Program
    {
        static void Main(string[] args)
        {

            Console.WriteLine("digite seu CEP: ");
            string Cep = Console.ReadLine();
            string url = "https://viacep.com.br/ws/" + Cep + "/json/";
             APIviaCEP apiviaCEP = BuscarAPIviaCEP(url);


           

            

            Console.WriteLine("Endereco");
            Console.WriteLine(
                String.Format("CEP: {0} - Logradouro: {1} - Complemento: {2} - Bairro: {3} - Localidade: {4} - UF: {5} - Unidade: {6} - IBGE: {7} - GIA: {8}",
                apiviaCEP.CEP, apiviaCEP.Logradouro, apiviaCEP.Complemento, apiviaCEP.Bairro, apiviaCEP.Localidade, apiviaCEP.UF, apiviaCEP.Unidade, apiviaCEP.IBGE, apiviaCEP.GIA));
            Console.WriteLine("\n\n");
            
            Console.WriteLine(apiviaCEP.CEP);
            
            Console.ReadLine();
        
        }

        
        public static APIviaCEP BuscarAPIviaCEP(string url)
        {
            WebClient wc = new WebClient();
            string content = wc.DownloadString(url);

            Console.WriteLine(content);
            Console.WriteLine("\n\n");

            APIviaCEP apiviaCEP  = JsonConvert.DeserializeObject<APIviaCEP>(content);

            return apiviaCEP;


        }
    }
}
