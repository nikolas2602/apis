﻿using Newtonsoft.Json;
using System;
using System.Net;
using UsuariosAleatorios.Models;

namespace UsuariosAleatorios
{
    class Program
    {
        static void Main(string[] args)
        {

            Console.WriteLine("digite o numero de usuarios que procura: ");
            string Numero = Console.ReadLine();
            Console.WriteLine("digite a nascionalidade do usuario: ");
            string Nasc = Console.ReadLine();
            
            string url = "https://randomuser.me/api/?results=" + Numero + "&nat" + Nasc ;
            RandomUsers random = BuscarRandom(url);
            Console.WriteLine("\n\n");
            
            Console.WriteLine(random.Results);



          



            foreach (Result Result in random.Results)
            {
                Console.WriteLine("Nomes");
                Console.WriteLine("\n");
                Console.WriteLine(Result.Name.Title);
                Console.WriteLine(Result.Name.First);
                Console.WriteLine(Result.Name.Last);
                Console.WriteLine("\n\n");
                Console.WriteLine("Email");
                Console.WriteLine("\n");
                Console.WriteLine(Result.Email);
                Console.WriteLine("\n\n");
                Console.WriteLine("Genero");
                Console.WriteLine("\n");
                Console.WriteLine(Result.Gender);
                Console.WriteLine("\n\n");
                Console.WriteLine("Localização");
                Console.WriteLine("\n");
                Console.WriteLine(Result.Location.Street.Name);
                Console.WriteLine(Result.Location.Street.Number);
                Console.WriteLine(Result.Location.City);
                Console.WriteLine(Result.Location.State);
                Console.WriteLine(Result.Location.Country);
                Console.WriteLine(Result.Location.Postcode);
                Console.WriteLine(Result.Location.Coordinates.Latitude);
                Console.WriteLine(Result.Location.Coordinates.Longitude);
                Console.WriteLine(Result.Location.Timezone.Offset);
                Console.WriteLine(Result.Location.Timezone.Description);
                Console.WriteLine("\n\n");

            }

            Console.ReadLine();

        }

        

    public static RandomUsers BuscarRandom(string url)
        {
            WebClient wc = new WebClient();
            string content = wc.DownloadString(url);

            Console.WriteLine(content);
            Console.WriteLine("\n\n");

            RandomUsers random = JsonConvert.DeserializeObject<RandomUsers>(content);

            return random;


        }
    }
    
}
