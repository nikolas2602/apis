﻿using System;
using System.Collections.Generic;
using System.Text;

namespace teste.Models
{
    class Computador
    {
        public string Marca { get; set; }
        public string Modelo { get; set; }
        public float Memoria { get; set; }
        public float Ssd { get; set; }
        public string Processador { get; set; }
        public List<string> Softwares { get; set; }
        public Fornecedor Fornecedor { get; set; }

    }

    class Fornecedor
    {
        public string RSocial { get; set; }
        public string Telefone { get; set; }
        public string Endereco { get; set; }
    }
}
