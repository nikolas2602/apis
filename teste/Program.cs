﻿using Newtonsoft.Json;
using System;
using System.Net;
using teste.Models;

namespace teste
{
    class Program
    {
        static void Main(string[] args)
        {
            //url da API
            string url = "http://senacao.tk/objetos/computador_array_objeto";
            //consumindo API
            Computador Computador = BuscarComputador(url);
            Console.WriteLine("Peças");
            Console.WriteLine(
                String.Format("Marca: {0} - Modelo: {1} - Memoria: {2} - Ssd: {3}", 
                Computador.Marca, Computador.Modelo, Computador.Memoria, Computador.Ssd));
            Console.WriteLine("\n\n");
            Console.WriteLine("Fornecedor");
            Console.WriteLine(
                String.Format("RSocial: {0}, Telefone: {1}, Endereco: {2}",
                Computador.Fornecedor.RSocial, Computador.Fornecedor.Telefone, Computador.Fornecedor.Endereco));
            Console.WriteLine("\n\n");
            Console.WriteLine("Softwares");


            foreach (string software in Computador.Softwares)
            {
                Console.WriteLine(software);
                
            }

            //Console.WriteLine(Computador.Softwares.ToString());

            Console.ReadLine();
        }

        public static Computador BuscarComputador(string url)
        {
            WebClient wc = new WebClient();
            string content = wc.DownloadString(url);

            Console.WriteLine(content);
            Console.WriteLine("\n\n");

            Computador Computador = JsonConvert.DeserializeObject<Computador>(content);

            return Computador;
        }
    }
}
